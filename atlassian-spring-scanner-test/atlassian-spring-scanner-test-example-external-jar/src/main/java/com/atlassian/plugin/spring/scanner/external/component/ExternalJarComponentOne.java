package com.atlassian.plugin.spring.scanner.external.component;

import org.springframework.stereotype.Component;

/**
 * This is a component that is defined in a jar OUTSIDE out source and hence outside the range of the annotation processor
 * and byte code scanner.  But @ClasspathComponent is meant t fix this up.
 */
@Component
public class ExternalJarComponentOne {
}
