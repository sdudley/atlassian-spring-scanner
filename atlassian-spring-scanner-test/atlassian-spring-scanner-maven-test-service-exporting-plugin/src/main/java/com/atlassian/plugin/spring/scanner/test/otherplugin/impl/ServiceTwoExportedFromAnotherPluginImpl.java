package com.atlassian.plugin.spring.scanner.test.otherplugin.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.test.otherplugin.ServiceTwoExportedFromAnotherPlugin;
import org.springframework.stereotype.Component;

/**
 * This export is {@code ComponentImport}ed by the atlassian-spring-scanner-maven-test plugin
 */
@ExportAsService(ServiceTwoExportedFromAnotherPlugin.class)
@Component
@SuppressWarnings("unused")
public class ServiceTwoExportedFromAnotherPluginImpl implements ServiceTwoExportedFromAnotherPlugin {
}
