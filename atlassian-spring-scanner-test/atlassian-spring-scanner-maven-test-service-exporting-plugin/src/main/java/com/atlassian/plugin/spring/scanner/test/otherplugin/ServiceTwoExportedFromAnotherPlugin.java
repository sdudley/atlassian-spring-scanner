package com.atlassian.plugin.spring.scanner.test.otherplugin;

/**
 * Service exported from this plugin, for testing {@code ComponentImport} and {@code ExportAsService}.
 */
public interface ServiceTwoExportedFromAnotherPlugin {
}
