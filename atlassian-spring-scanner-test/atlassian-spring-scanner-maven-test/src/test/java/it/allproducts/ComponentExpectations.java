package it.allproducts;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.equalTo;

/**
 * Abstracts expected components across {@link TestPackaging} and {@link it.perproduct.AbstractComponentsInProductTest}
 * subclasses - ensures our packaging tests match our runtime tests, etc.
 *
 * The expectations in this class reflect the {@link Component}, {@link ComponentImport}, {@link ExportAsService} etc.
 * in the test plugin.
 */
public class ComponentExpectations {

    public static final ComponentExpectations COMMON = new ComponentExpectations();

    public static final ComponentExpectations REFAPP = new ComponentExpectations(
            // RefappComponent
            component("com.atlassian.plugin.spring.scanner.test.product.refapp.RefappOnlyComponent"),
            // RefappImport
            componentImport("com.atlassian.refapp.api.ConnectionProvider"),
            // Extra components that only appear in Refapp
            ImmutableList.of(autoAddedComponent("org.springframework.context.event.internalEventListenerFactory"),
                             autoAddedComponent("org.springframework.context.event.internalEventListenerProcessor")));

    public static final ComponentExpectations CONFLUENCE = new ComponentExpectations(
            // ConfluenceComponent
            component("com.atlassian.plugin.spring.scanner.test.product.confluence.ConfluenceOnlyComponent"),
            // ConfluenceImport
            componentImport("com.atlassian.confluence.api.service.content.ContentService"),
            // Extra components that only appear in Confluence
            ImmutableList.of(autoAddedComponent("org.springframework.context.event.internalEventListenerFactory"),
                             autoAddedComponent("org.springframework.context.event.internalEventListenerProcessor")));

    public static final ComponentExpectations JIRA_CLOUD = new ComponentExpectations(
            // JiraComponent
            component("com.atlassian.plugin.spring.scanner.test.product.jira.JiraOnlyComponent"),
            // JiraImport
            componentImport("com.atlassian.jira.bc.issue.comment.CommentService"),
            // Extra components that only appear in JIRA Cloud
            ImmutableList.of(autoAddedComponent("org.springframework.context.event.internalEventListenerFactory"),
                             autoAddedComponent("org.springframework.context.event.internalEventListenerProcessor")));

    public static final ComponentExpectations JIRA_BTF = new ComponentExpectations(
            // JiraComponent
            component("com.atlassian.plugin.spring.scanner.test.product.jira.JiraOnlyComponent"),
            // JiraImport
            componentImport("com.atlassian.jira.bc.issue.comment.CommentService"));

    public static final ComponentExpectations BAMBOO = new ComponentExpectations(
            // BambooComponent
            component("com.atlassian.plugin.spring.scanner.test.product.bamboo.BambooOnlyComponent"),
            // BambooImport
            componentImport("com.atlassian.bamboo.build.BuildExecutionManager"));

    public static final ComponentExpectations STASH = new ComponentExpectations(
            // StashComponent
            component("com.atlassian.plugin.spring.scanner.test.product.stash.StashOnlyComponent"),
            // StashImport
            componentImport("com.atlassian.stash.repository.RepositoryService"));

    public static final ComponentExpectations BITBUCKET = new ComponentExpectations(
            // BitbucketComponent
            component("com.atlassian.plugin.spring.scanner.test.product.bitbucket.BitbucketOnlyComponent"),
            // BitbucketImport
            componentImport("com.atlassian.bitbucket.repository.RepositoryService"));

    public static final ComponentExpectations FECRU = new ComponentExpectations(
            // FecruComponent
            component("com.atlassian.plugin.spring.scanner.test.product.fecru.FecruOnlyComponent"),
            // FecruImport
            componentImport("com.atlassian.fisheye.spi.services.RepositoryService"));

    private final List<ExpectedComponent> productSpecificScannerCreatedComponents;
    private final List<ExpectedAutoAddedComponent> productSpecificAutoAddedComponents;
    private final List<ExpectedComponentImport> productSpecificComponentImports;

    private ComponentExpectations(ExpectedComponent productSpecificComponent,
                                  ExpectedComponentImport productSpecificComponentImport,
                                  List<ExpectedAutoAddedComponent> productSpecificAutoAddedComponents) {
        this.productSpecificScannerCreatedComponents = ImmutableList.of(productSpecificComponent);
        this.productSpecificComponentImports = ImmutableList.of(productSpecificComponentImport);
        this.productSpecificAutoAddedComponents = productSpecificAutoAddedComponents;
    }

    private ComponentExpectations(ExpectedComponent productSpecificComponent,
                                  ExpectedComponentImport productSpecificComponentImport) {
        this(productSpecificComponent, productSpecificComponentImport, emptyList());
    }

    private ComponentExpectations() {
        this.productSpecificScannerCreatedComponents = emptyList();
        this.productSpecificComponentImports = emptyList();
        this.productSpecificAutoAddedComponents = emptyList();
    }

    /**
     * Things in META-INF/plugin-components/components
     */
    public List<ExpectedComponent> getExpectedScannerCreatedComponents() {
        return ImmutableList.<ExpectedComponent>builder()
                // Test plugin's components instantiated in different ways
                .add(component("com.atlassian.plugin.spring.scanner.test.ConsumingInternalOnlyComponent"))
                .add(component("com.atlassian.plugin.spring.scanner.test.imported.ConsumingMixedComponents"))
                .add(component("com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponent"))
                .add(component("com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentWithSpecifiedInterface"))
                .add(component("com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentWithMultipleSpecifiedInterfaces"))
                .add(component("com.atlassian.plugin.spring.scanner.test.exported.ExposedAsADevServiceComponent"))
                .add(component("com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalServiceViaField"))
                .add(component("com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalDevServiceViaField"))
                .add(component("com.atlassian.plugin.spring.scanner.test.InternalComponent"))
                .add(component("com.atlassian.plugin.spring.scanner.test.InternalComponentTwo"))
                .add(component("com.atlassian.plugin.spring.scanner.test.NamedComponent", "namedComponent"))
                .add(component("com.atlassian.plugin.spring.scanner.test.imported.NamedConsumingMixedComponents", "namedMixed"))
                .add(component("com.atlassian.plugin.spring.scanner.test.OuterClass$InnerClass$InnerComponent$EvenMoreInnerComponent"))
                .add(component("com.atlassian.plugin.spring.scanner.test.OuterClass$InnerClass$InnerComponent"))
                .add(component("com.atlassian.plugin.spring.scanner.test.OuterClass$OuterComponent"))
                .add(component("com.atlassian.plugin.spring.scanner.test.primary.BaseImplementation"))
                .add(component("com.atlassian.plugin.spring.scanner.test.primary.PrimaryImplementation"))
                .add(component("com.atlassian.plugin.spring.scanner.test.primary.ConsumingPrimaryComponentByInterface"))

                .add(component("com.atlassian.plugin.spring.scanner.test.fields.ComponentWithFields"))

                .add(component("com.atlassian.plugin.spring.scanner.test.spring.ControllerComponent"))
                .add(component("com.atlassian.plugin.spring.scanner.test.spring.RepositoryComponent"))
                .add(component("com.atlassian.plugin.spring.scanner.test.spring.ServiceComponent"))

                .add(component("com.atlassian.plugin.spring.scanner.test.jsr.JsrComponent"))

                .add(component("com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentOne"))
                .add(component("com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentTwo"))
                .add(component("com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentComposite"))
                .add(component("com.atlassian.plugin.spring.scanner.external.notannotated.NotAnnotatedExternalJarClass"))

                .add(component("com.atlassian.plugin.spring.scanner.external.component.TransitiveJarComponent1"))
                .add(component("com.atlassian.plugin.spring.scanner.external.component.TransitiveJarComponent2"))
                .add(component("com.atlassian.plugin.spring.scanner.external.component.TransitiveJarComponentComposite"))

                .add(component("com.atlassian.plugin.spring.scanner.test.imported.ConsumesExternalComponentsViaConstructor"))
                .add(component("com.atlassian.plugin.spring.scanner.test.imported.ConsumesExternalComponentsViaFields"))

                // Test plugin's components - test infrastructure
                .add(component("com.atlassian.plugin.spring.scanner.test.dynamic.DynamicContextManager"))
                .add(component("com.atlassian.plugin.spring.scanner.test.registry.BeanLister"))

                // Module type
                .add(component("com.atlassian.plugin.spring.scanner.test.moduletype.BasicModuleTypeFactory"))
                // because we have a @ModuleType, we get an extra component in support of that
                .add(component("com.atlassian.plugin.osgi.bridge.external.SpringHostContainer"))
                .build();
    }

    /**
     * Common components instantiated by scanner programmatically: don't appear in META-INF/plugin-components
     */
    public List<ExpectedAutoAddedComponent> getExpectedAutoAddedComponents() {
        return ImmutableList.<ExpectedAutoAddedComponent>builder()
                // Scanner's internal components auto-added to spring context
                .add(autoAddedComponent("devModeBeanInitialisationLoggerBeanPostProcessor", "com.atlassian.plugin.spring.scanner.runtime.impl.DevModeBeanInitialisationLoggerBeanPostProcessor"))
                .add(autoAddedComponent("serviceExportBeanPostProcessor", "com.atlassian.plugin.spring.scanner.runtime.impl.ServiceExporterBeanPostProcessor"))
                .add(autoAddedComponent("componentImportBeanFactoryPostProcessor", "com.atlassian.plugin.spring.scanner.runtime.impl.ComponentImportBeanFactoryPostProcessor"))

                // Spring internal components
                .add(autoAddedComponent("org.springframework.context.annotation.internalAutowiredAnnotationProcessor", "org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor"))
                .add(autoAddedComponent("org.springframework.context.annotation.internalCommonAnnotationProcessor", "org.springframework.context.annotation.CommonAnnotationBeanPostProcessor"))
                .add(autoAddedComponent("org.springframework.context.annotation.internalConfigurationAnnotationProcessor", "org.springframework.context.annotation.ConfigurationClassPostProcessor"))
                .add(autoAddedComponent("org.springframework.context.annotation.internalRequiredAnnotationProcessor", "org.springframework.beans.factory.annotation.RequiredAnnotationBeanPostProcessor"))
                .build();
    }

    /**
     * Things in META-INF/plugin-components/imports
     */
    public List<ExpectedComponentImport> getExpectedScannerCreatedComponentImports() {
        return ImmutableList.<ExpectedComponentImport>builder()
                .add(componentImport("com.atlassian.event.api.EventPublisher"))
                .add(componentImport("com.atlassian.plugin.PluginAccessor"))
                .add(componentImport("com.atlassian.plugin.module.ModuleFactory"))
                .add(componentImport("com.atlassian.plugin.spring.scanner.test.otherplugin.ServiceExportedFromAnotherPlugin"))
                // .add(componentImport("ServiceTwoExportedFromAnotherPlugin")) is only in dynamic profile
                .add(componentImport("com.atlassian.plugin.spring.scanner.test.otherplugin.ServiceTwoExportedFromAnotherPlugin"))
                .build();
    }

    /**
     * Things in META-INF/plugin-components/exports
     */
    public List<ExpectedServiceExport> getExpectedScannerCreatedExports() {
        return ImmutableList.<ExpectedServiceExport>builder()
                // Explicitly exported services
                .add(serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponent",
                        ImmutableList.of(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterface")))
                .add(serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentWithSpecifiedInterface",
                        ImmutableList.of(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterfaceThree"),
                        ImmutableList.of(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterfaceThree")))
                .add(serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentWithMultipleSpecifiedInterfaces",
                        ImmutableList.of(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterface",
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterfaceTwo"),
                        ImmutableList.of(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterface",
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterfaceTwo")))
                .add(serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalServiceViaField",
                        ImmutableList.of(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalServiceViaField")))

                // Module type
                .add(serviceExport("com.atlassian.plugin.spring.scanner.test.moduletype.BasicModuleTypeFactory",
                        ImmutableList.of(
                                "com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory"),
                        ImmutableList.of(
                                "com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory")))
                .build();
    }

    /**
     * Things in META-INF/plugin-components/dev-exports
     */
    public List<ExpectedServiceExport> getExpectedScannerCreatedDevExports() {
        return ImmutableList.of(
                serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsADevServiceComponent",
                        ImmutableList.of(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsADevServiceComponent")),
                serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalDevServiceViaField",
                        ImmutableList.of(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalDevServiceViaField")));
    }

    /**
     * Common stuff made by scanner programmatically: don't appear in META-INF/plugin-components
     */
    public List<ExpectedServiceExport> getExpectedAutoAddedExports() {
        return ImmutableList.of(
                // Gemini's common services
                serviceExport(null,
                        ImmutableList.<String>builder()
                            .add("org.eclipse.gemini.blueprint.context.DelegatedExecutionOsgiBundleApplicationContext")
                            .add("org.eclipse.gemini.blueprint.context.ConfigurableOsgiBundleApplicationContext")
                            .add("org.springframework.context.ConfigurableApplicationContext")
                            .add("org.springframework.context.ApplicationContext")
                            .add("org.springframework.context.Lifecycle")
                            .add("java.io.Closeable")
                            .add("org.springframework.beans.factory.ListableBeanFactory")
                            .add("org.springframework.beans.factory.HierarchicalBeanFactory")
                            .add("org.springframework.context.MessageSource")
                            .add("org.springframework.context.ApplicationEventPublisher")
                            .add("org.springframework.core.io.support.ResourcePatternResolver")
                            .add("org.springframework.beans.factory.BeanFactory")
                            .add("org.springframework.core.io.ResourceLoader")
                            .add("java.lang.AutoCloseable")
                            .add("org.springframework.beans.factory.DisposableBean")
                            .build()));
    }

    /**
     * Things in META-INF/plugin-components/profile-dynamic/component
     */
    public List<ExpectedComponent> getExpectedScannerCreatedDynamicComponents() {
        return ImmutableList.of(component("com.atlassian.plugin.spring.scanner.test.dynamic.DynamicComponent"));
    }

    /**
     * Things in META-INF/plugin-components/profile-dynamic/imports
     */
    public List<ExpectedComponentImport> getExpectedScannerCreatedDynamicComponentImports() {
        return ImmutableList.of(
                componentImport("com.atlassian.plugin.spring.scanner.test.otherplugin.DynamicallyImportedServiceFromAnotherPlugin"));
    }

    /**
     * Things in META-INF/plugin-components/component-{product}
     */
    public List<ExpectedComponent> getProductSpecificScannerCreatedComponents() {
        return productSpecificScannerCreatedComponents;
    }

    /**
     * Things in META-INF/plugin-components/imports-{product}
     */
    public List<ExpectedComponentImport> getProductSpecificScannerCreatedComponentImports() {
        return productSpecificComponentImports;
    }

    /**
     * Product-specific stuff made by product programmatically: don't appear in META-INF/component-{product}
     */
    public List<ExpectedAutoAddedComponent> getProductSpecificAutoAddedComponents() {
        return productSpecificAutoAddedComponents;
    }

    private static String toComponentName(String classNameWithPackage) {
        final String simpleClassName = classNameWithPackage.replaceAll(".*\\.", "").replace('$', '.');
        return simpleClassName.substring(0, 1).toLowerCase() + simpleClassName.substring(1);
    }

    private static ExpectedComponent component(String implClass) {
        return new ExpectedComponent(implClass, null);
    }

    private static ExpectedComponent component(String implClass, String specifiedName) {
        return new ExpectedComponent(implClass, specifiedName);
    }

    private static ExpectedComponentImport componentImport(String iface) {
        return new ExpectedComponentImport(iface);
    }

    private static ExpectedAutoAddedComponent autoAddedComponent(String name) {
        return new ExpectedAutoAddedComponent(name);
    }

    private static ExpectedAutoAddedComponent autoAddedComponent(String name, String implClass) {
        return new ExpectedAutoAddedComponent(name, implClass);
    }

    private static ExpectedServiceExport serviceExport(String implClass, List<String> expectedExportedInterfaces) {
        return new ExpectedServiceExport(implClass, expectedExportedInterfaces, null);
    }

    private static ExpectedServiceExport serviceExport(String implClass, List<String> expectedExportedInterfaces, List<String> specifiedInterfaces) {
        return new ExpectedServiceExport(implClass, expectedExportedInterfaces, specifiedInterfaces);
    }

    public static abstract class AbstractExpectedComponent {
        /**
         * What do we expect to appear in META-INF/plugin-components index file lines?
         *
         * @see TestPackaging
         */
        public abstract Matcher<String> getIndexLineMatcher();

        /**
         * What do we expect to appear as a runtime component
         * as output by {@link com.atlassian.plugin.spring.scanner.test.servlet.ComponentStatusServlet}?
         *
         * @see it.perproduct.AbstractComponentsInProductTest
         */
        public abstract Matcher<String> getRuntimeComponentEntryMatcher();
    }

    public static class ExpectedComponent extends AbstractExpectedComponent {
        private final String implClass;
        private final String specifiedName;

        public ExpectedComponent(String implClass, String specifiedName) {
            this.implClass = implClass;
            this.specifiedName = specifiedName;
        }

        @Override
        public Matcher<String> getIndexLineMatcher() {
            return equalTo(implClass + ((specifiedName == null) ? "" : "#" + specifiedName));
        }

        @Override
        public Matcher<String> getRuntimeComponentEntryMatcher() {
            return equalTo(((specifiedName != null) ? specifiedName : toComponentName(implClass))
                    + " = " + implClass);
        }
    }

    public static class ExpectedComponentImport extends AbstractExpectedComponent {
        private final String iface;

        private ExpectedComponentImport(String iface) {
            this.iface = iface;
        }

        @Override
        public Matcher<String> getIndexLineMatcher() {
            return equalTo(iface);
        }

        @Override
        public Matcher<String> getRuntimeComponentEntryMatcher() {
            // Don't care much about actual imported impl class - some of them vary by product
            return new AnyImplClassComponentMatcher(toComponentName(iface));
        }
    }

    public static class ExpectedAutoAddedComponent extends AbstractExpectedComponent {
        private final String name;
        private final String implClass;

        private ExpectedAutoAddedComponent(String name) {
            this.name = name;
            this.implClass = null;
        }

        private ExpectedAutoAddedComponent(String name, String implClass) {
            this.name = name;
            this.implClass = implClass;
        }

        @Override
        public Matcher<String> getIndexLineMatcher() {
            // Created at runtime, doesn't appear in index files
            return null;
        }

        @Override
        public Matcher<String> getRuntimeComponentEntryMatcher() {
            if (implClass == null) {
                // Don't care much about actual imported impl class - some of them vary by product
                return new AnyImplClassComponentMatcher(name);
            }
            return equalTo(name + " = " + implClass);
        }
    }

    public static class ExpectedServiceExport extends AbstractExpectedComponent {
        private final String implClass;
        private final List<String> expectedExportedInterfaces;
        private final List<String> specifiedInterfaces;

        private ExpectedServiceExport(String implClass,
                                      List<String> expectedExportedInterfaces,
                                      List<String> specifiedInterfaces) {
            this.implClass = implClass;
            this.expectedExportedInterfaces = expectedExportedInterfaces;
            this.specifiedInterfaces = specifiedInterfaces;
        }

        @Override
        public Matcher<String> getIndexLineMatcher() {
            return equalTo(implClass +
                    ((specifiedInterfaces != null)
                            ? ("#" + Joiner.on(",").join(specifiedInterfaces))
                            : ""));
        }

        @Override
        public Matcher<String> getRuntimeComponentEntryMatcher() {
            if (expectedExportedInterfaces == null) {
                return equalTo(implClass);
            }
            return new InterfaceMatcherInAnyOrder(expectedExportedInterfaces);
        }
    }

    private static class InterfaceMatcherInAnyOrder extends TypeSafeMatcher<String> {
        private final List<String> interfaces;

        public InterfaceMatcherInAnyOrder(List<String> interfaces) {
            this.interfaces = interfaces;
        }

        @Override
        protected boolean matchesSafely(String item) {
            // grr hamcrest generics
            Collection<Matcher<? super String>> expected = interfaces.stream()
                    .map(Matchers::equalTo)
                    .collect(Collectors.toList());
            Iterable<? extends String> actual = ImmutableList.<String>builder().add(item.split(",")).build();
            final Matcher<Iterable<? extends String>> iterableMatcher
                    = Matchers.containsInAnyOrder(expected);
            return iterableMatcher.matches(actual);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("exported interfaces: '" + interfaces + "'");
        }
    }

    /**
     * Match expected components by name only, coping with implementation classes that vary between products.
     */
    private static class AnyImplClassComponentMatcher extends TypeSafeMatcher<String> {

        private final String componentName;

        public AnyImplClassComponentMatcher(String componentName) {
            this.componentName = componentName;
        }

        @Override
        protected boolean matchesSafely(String item) {
            // Wildcards for cross-product variability in impl classes: e.g. matches both:
            //   eventPublisher = com.atlassian.event.internal.EventPublisherImpl
            //   eventPublisher = com.atlassian.confluence.event.TimingEventPublisher
            return item.startsWith(componentName + " = ");
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("component named: '" + componentName + "' (any impl)");
        }
    }
}
