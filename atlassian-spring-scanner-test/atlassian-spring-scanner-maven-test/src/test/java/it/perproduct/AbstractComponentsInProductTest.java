package it.perproduct;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.test.servlet.ComponentStatusServlet;
import com.atlassian.plugin.spring.scanner.test.servlet.ManageDynamicContextServlet;
import com.google.common.collect.ImmutableList;
import it.allproducts.ComponentExpectations;
import it.allproducts.ComponentExpectations.AbstractExpectedComponent;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertThat;

/**
 * Base class for integration tests of component-related functionality common to all products,
 * such as
 * {@link ComponentImport},
 * {@link ExportAsService},
 * and the dynamic context creation / profile support.
 * (i.e. everything that's not a product-specific component or import)
 *
 * See also product-specific subclasses.
 * See {@link it.allproducts.TestCustomModuleType}.
 */
public abstract class AbstractComponentsInProductTest extends AbstractInProductTest {

    /**
     * @see ComponentStatusServlet
     */
    private static final String COMPONENT_STATUS_URL = BASEURL + "/plugins/servlet/component-status?components";

    /**
     * @see ComponentStatusServlet
     */
    private static final String SERVICE_STATUS_URL = BASEURL + "/plugins/servlet/component-status?services";

    /**
     * @see ManageDynamicContextServlet
     */
    private static final String START_DYNAMIC_CONTEXT_URL = BASEURL + "/plugins/servlet/manage-dynamic-contexts?startup";
    private static final String STOP_DYNAMIC_CONTEXT_URL = BASEURL + "/plugins/servlet/manage-dynamic-contexts?shutdown";

    /**
     * Test that {@link Component}, {@link ComponentImport} etc. are working in atlassian-spring-scanner-maven-test.
     */
    @Test
    public void testComponents() throws Exception {
        List<String> actualComponentsInProduct = readStringList(COMPONENT_STATUS_URL);
        List<AbstractExpectedComponent> expected = getExpectedComponentsWithDefaultProfile();

        assertThat(actualComponentsInProduct, hasComponents(expected));
    }

    /**
     * Test that {@link ExportAsService} etc. are working in atlassian-spring-scanner-maven-test.
     */
    @Test
    public void testServices() throws Exception {
        List<String> actualServicesInProduct = readStringList(SERVICE_STATUS_URL);
        List<AbstractExpectedComponent> expected = getExpectedServiceExportsWithDefaultProfile();

        assertThat(actualServicesInProduct, hasComponents(expected));
    }

    /**
     * Test dynamic creation and shutdown of inner spring context (profiles)
     */
    @Test
    public void testDynamicContext() throws Exception {
        // Initially, we're running with the default profile, so we get the default profile's components and services (same as other tests)

        List<AbstractExpectedComponent> expectedComponentsDefaultProfile = getExpectedComponentsWithDefaultProfile();
        List<AbstractExpectedComponent> expectedServicesDefaultProfile = getExpectedServiceExportsWithDefaultProfile();

        assertComponentsAndServices(expectedComponentsDefaultProfile, expectedServicesDefaultProfile);

        // Enable the dynamic context. This should load DynamicComponent.
        getUrl(START_DYNAMIC_CONTEXT_URL);

        // When the dynamic profile is activated, we expect a new component and a new import to appear.
        // No change in exported services.
        List<AbstractExpectedComponent> expectedDynamicComponents =
                ImmutableList.<AbstractExpectedComponent>builder()
                        .addAll(expectedComponentsDefaultProfile)
                        .addAll(ComponentExpectations.COMMON.getExpectedScannerCreatedDynamicComponents())
                        .addAll(ComponentExpectations.COMMON.getExpectedScannerCreatedDynamicComponentImports())
                        .build();

        assertComponentsAndServices(expectedDynamicComponents, expectedServicesDefaultProfile);

        // Stop the dynamic context - we should then return to the starting state - only default profile's components and services
        getUrl(STOP_DYNAMIC_CONTEXT_URL);

        assertComponentsAndServices(expectedComponentsDefaultProfile, expectedServicesDefaultProfile);
    }

    private void assertComponentsAndServices(List<AbstractExpectedComponent> expectedComponents,
                                             List<AbstractExpectedComponent> expectedServices)
            throws IOException {
        List<String> actualComponents = readStringList(COMPONENT_STATUS_URL);
        List<String> actualServices = readStringList(SERVICE_STATUS_URL);

        assertThat(actualComponents, hasComponents(expectedComponents));
        assertThat(actualServices, hasComponents(expectedServices));
    }

    abstract ComponentExpectations getProductSpecificExpectations();

    private List<AbstractExpectedComponent> getExpectedComponentsWithDefaultProfile() {
        final ComponentExpectations productSpecificExpectations = getProductSpecificExpectations();

        return ImmutableList.<AbstractExpectedComponent>builder()
                // Common to all products
                .addAll(productSpecificExpectations.getExpectedScannerCreatedComponents()) // includes those exported
                .addAll(productSpecificExpectations.getExpectedScannerCreatedComponentImports())
                .addAll(productSpecificExpectations.getExpectedAutoAddedComponents())
                // Product-specific
                .addAll(productSpecificExpectations.getProductSpecificScannerCreatedComponents())
                .addAll(productSpecificExpectations.getProductSpecificScannerCreatedComponentImports())
                .addAll(productSpecificExpectations.getProductSpecificAutoAddedComponents())
                .build();
    }

    private List<AbstractExpectedComponent> getExpectedServiceExportsWithDefaultProfile() {
        // Our test plugin current has the same exports in all products
        final ComponentExpectations expectations = ComponentExpectations.COMMON;

        final ImmutableList.Builder<AbstractExpectedComponent> builder =
                ImmutableList.<AbstractExpectedComponent>builder()
                    .addAll(expectations.getExpectedScannerCreatedExports())
                    .addAll(expectations.getExpectedAutoAddedExports());
        // If we're running in dev mode (which is the AMPS default if this property isn't set), the dev exports should be present, else not
        if (Boolean.parseBoolean(System.getProperty("atlassian.dev.mode", "true"))) {
            builder.addAll(expectations.getExpectedScannerCreatedDevExports());
        }
        return builder.build();
    }

    private static Matcher<Iterable<? extends String>> hasComponents(List<AbstractExpectedComponent> expectedComponents) {
        Collection<Matcher<? super String>> componentMatchers =
                expectedComponents.stream()
                        .map(AbstractExpectedComponent::getRuntimeComponentEntryMatcher)
                        .collect(Collectors.toList());
        return Matchers.containsInAnyOrder(componentMatchers);
    }
}
