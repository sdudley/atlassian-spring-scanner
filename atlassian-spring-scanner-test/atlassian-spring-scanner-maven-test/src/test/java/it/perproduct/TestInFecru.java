package it.perproduct;

import it.allproducts.ComponentExpectations;

/**
 * Test component imports and exports of the test plugin when actually installed in the product.
 */
public class TestInFecru extends AbstractComponentsInProductTest {

    @Override
    ComponentExpectations getProductSpecificExpectations() {
        return ComponentExpectations.FECRU;
    }
}
