package com.atlassian.plugin.spring.scanner.test.imported;

import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentComposite;

public class ClasspathImporter {
    @ClasspathComponent
    ExternalJarComponentComposite externalJarComponentComposite;


    /*

    We reply on the extra jar scanning here to find these guys.  Neat eh!

    @ClasspathComponent
    ExternalJarComponentOne externalJarComponentOne;
    @ClasspathComponent
    ExternalJarComponentTwo externalJarComponentTwo;
    */

}
