package com.atlassian.plugin.spring.scanner.test.product.stash;

import com.atlassian.plugin.spring.scanner.annotation.component.StashComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.StashImport;
import com.atlassian.stash.repository.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A component that's only instantiated when running in Stash
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@StashComponent
public class StashOnlyComponent {
    private final RepositoryService stashRepositoryService;

    @Autowired
    public StashOnlyComponent(@StashImport RepositoryService stashRepositoryService) {
        this.stashRepositoryService = stashRepositoryService;
    }
}
