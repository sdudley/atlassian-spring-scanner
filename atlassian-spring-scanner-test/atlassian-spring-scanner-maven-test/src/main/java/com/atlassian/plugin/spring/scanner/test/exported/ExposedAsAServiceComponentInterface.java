package com.atlassian.plugin.spring.scanner.test.exported;

public interface ExposedAsAServiceComponentInterface {
    void doStuff();
}
