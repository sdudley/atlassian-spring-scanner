package com.atlassian.plugin.spring.scanner.test.product.fecru;

import com.atlassian.fisheye.spi.services.RepositoryService;
import com.atlassian.plugin.spring.scanner.annotation.component.FecruComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.FecruImport;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A component that's only instantiated when running in Fecru
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@FecruComponent
public class FecruOnlyComponent {

    private final RepositoryService fecruRepositoryService;

    @Autowired
    public FecruOnlyComponent(@FecruImport RepositoryService fecruRepositoryService) {
        this.fecruRepositoryService = fecruRepositoryService;
    }
}
