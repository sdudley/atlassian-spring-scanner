package com.atlassian.plugin.spring.scanner.test.dynamic;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.dynamic.contexts.DynamicContext;
import com.google.common.base.Joiner;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

/**
 * This magic little class can dynamically cause other components to come into and out of existence.
 */
@SuppressWarnings({"SpringJavaAutowiringInspection", "FieldCanBeLocal", "UnusedDeclaration"})
@Component
public class DynamicContextManager {
    private static final String[] CONFIG_PATHS = {"/dynamicProfile/dynamic-context-spring-scanner.xml"};
    private static final Joiner INTERFACE_NAME_JOINER = Joiner.on(", ");

    private final ApplicationContext parentContext;
    private final BundleContext bundleContext;
    private final DynamicContext.Installer dynamicContextInstaller;
    private ConfigurableApplicationContext internalContext;

    @Autowired
    public DynamicContextManager(@ComponentImport EventPublisher eventPublisher, @ComponentImport PluginAccessor pluginAccessor, ApplicationContext parentContext, BundleContext bundleContext) {
        this.parentContext = parentContext;
        this.bundleContext = bundleContext;

        final String pluginKey = "com.atlassian.plugin.atlassian-spring-scanner-maven-test";
        Plugin plugin = pluginAccessor.getPlugin(pluginKey);
        if (plugin == null) {
            throw new IllegalStateException("Plugin with key '" + pluginKey + "' was not found");
        }

        dynamicContextInstaller = DynamicContext.installer(eventPublisher, bundleContext, plugin.getKey());
    }

    public Result bootstrapTheRestOfTheApplication() {
        long then = System.currentTimeMillis();
        if (internalContext == null) {
            this.internalContext = dynamicContextInstaller.useContext(CONFIG_PATHS, parentContext);

            return new Result(true, then);
        } else {
            return new Result(false, then);
        }
    }

    public Result shutdownNewContext() {
        long then = System.currentTimeMillis();
        if (internalContext != null) {
            dynamicContextInstaller.closeAndUseContext(internalContext, internalContext.getParent());
            internalContext = null;
            return new Result(true, then);
        }
        return new Result(false, then);
    }

    public ConfigurableApplicationContext getInternalContext() {
        return internalContext;
    }

    public static class Result {
        boolean tookPlace;
        long timeTaken;

        Result(final boolean tookPlace, final long then) {
            this.tookPlace = tookPlace;
            this.timeTaken = System.currentTimeMillis() - then;
        }

        public boolean tookPlace() {
            return tookPlace;
        }

        public long getTimeTaken() {
            return timeTaken;
        }
    }
}
