package com.atlassian.plugin.spring.scanner.test.servlet;

import com.atlassian.plugin.spring.scanner.test.dynamic.DynamicContextManager;
import com.atlassian.plugin.spring.scanner.test.registry.BeanLister;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

/**
 * Allows enabling and disabling of the dynamic inner application context.
 */
public class ManageDynamicContextServlet extends HttpServlet {
    private final DynamicContextManager bootstrappingComponent;
    private final BeanLister beanLister;

    public ManageDynamicContextServlet(final DynamicContextManager bootstrappingComponent,
                                       final BeanLister beanLister) {
        this.bootstrappingComponent = bootstrappingComponent;
        this.beanLister = beanLister;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();

        if (request.getParameter("startup") != null) {
            doStartup(response);
        } else if (request.getParameter("shutdown") != null) {
            doShutdown(response);
        } else {
            writeSuccess(response, "");
        }

        response.flushBuffer();
    }

    private void doStartup(HttpServletResponse response) throws IOException {
        DynamicContextManager.Result result = bootstrappingComponent.bootstrapTheRestOfTheApplication();
        if (result.tookPlace()) {
            writeSuccess(response, String.format("Bootstrapped....in %d ms", result.getTimeTaken()));
        } else {
            response.setStatus(500);
            throw new IllegalStateException("Oop...looks like we are already bootstrapped!");
        }
    }

    private void doShutdown(HttpServletResponse response) throws IOException {
        DynamicContextManager.Result result = bootstrappingComponent.shutdownNewContext();
        if (result.tookPlace()) {
            writeSuccess(response, String.format("The internal components have been shutdown in %d ms", result.getTimeTaken()));
        } else {
            response.setStatus(500);
            throw new IllegalStateException("The shutdown has already taken place");
        }
    }

    private void links(PrintWriter out) {
        out.print("</br></br><div>"
                + "<a href='manage-dynamic-contexts?startup'>Start dynamic context</a> "
                + "<a href='manage-dynamic-contexts?shutdown'>Shut down dynamic context</a> "
                + "<a href='component-status'>List components</a> "
                + "</div>");
    }

    private void writeSuccess(HttpServletResponse response, String body) throws IOException {
        response.setStatus(200);
        PrintWriter out = response.getWriter();
        out.print("<html><body>");
        out.print(body);

        displayBody(out, beanLister.listBeans(), beanLister.listServices());

        out.print("</body></html>");
    }

    private void displayBody(final PrintWriter out, final Set<String> beans, final Set<String> services) {
        displaySection(out, "The following components are defined in the app context:", beans);
        displaySection(out, "The following services are exported by this bundle:", services);
        links(out);
    }

    private void displaySection(final PrintWriter out, final String title, final Set<String> things) {
        out.print("<h3>" + title + "</h3>" + "<div><ul>");
        for (final String thing : things) {
            out.print(String.format("<li><pre>%s</pre></li>", thing));
        }
        out.print("</ul></div>");
    }
}
