package com.atlassian.plugin.spring.scanner.test.registry;

import com.atlassian.plugin.spring.scanner.test.dynamic.DynamicContextManager;
import com.google.common.base.Joiner;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Allows the test to list other components.
 */
@Component
public class BeanLister {

    private static final Joiner INTERFACE_NAME_JOINER = Joiner.on(",");

    private final DynamicContextManager bootstrappingComponent;
    private final ApplicationContext parentContext;
    private final BundleContext bundleContext;

    @Autowired
    public BeanLister(DynamicContextManager bootstrappingComponent,
                      ApplicationContext parentContext,
                      BundleContext bundleContext) {
        this.bootstrappingComponent = bootstrappingComponent;
        this.parentContext = parentContext;
        this.bundleContext = bundleContext;
    }

    public Set<String> listBeans() {
        Set<String> beans = new TreeSet<>();
        final ConfigurableApplicationContext internalContext = bootstrappingComponent.getInternalContext();
        if (internalContext != null) {
            beans.addAll(buildBeanDescriptions(internalContext));
        }
        beans.addAll(buildBeanDescriptions(parentContext));
        return beans;
    }

    private Set<String> buildBeanDescriptions(ApplicationContext context) {
        final Set<String> beans = new HashSet<>();
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        for (String name : beanDefinitionNames) {
            // Make something consistent we can match against in AbstractInProductTest.
            // We use toString() because it shows us the target of service proxies,
            // but strip the object ID suffix '@hexstuff'
            beans.add(name + " = " + context.getBean(name).toString().replaceFirst("@.*", ""));
        }
        return beans;
    }

    public Set<String> listServices() {
        final Set<String> services = new TreeSet<>();
        for (final ServiceReference serviceReference : bundleContext.getBundle().getRegisteredServices()) {
            final String[] interfaces = (String[]) serviceReference.getProperty(Constants.OBJECTCLASS);
            services.add(INTERFACE_NAME_JOINER.join(interfaces));
        }
        return services;
    }
}
