package com.atlassian.plugin.spring.scanner.test.servlet;

import com.atlassian.plugin.spring.scanner.test.registry.BeanLister;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Returns status of live components, so test can assert against them.
 */
public class ComponentStatusServlet extends HttpServlet {
    private final BeanLister beanLister;

    @Inject
    public ComponentStatusServlet(BeanLister beanLister) {
        this.beanLister = beanLister;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/plain");

        boolean components = (request.getParameter("components") != null);
        boolean services = (request.getParameter("services") != null);
        if (!components && !services) {
            components = true;
            services = true;
        }

        if (components) {
            for (final String name : beanLister.listBeans()) {
                response.getWriter().println(name);
            }
        }

        if (services) {
            for (final String name : beanLister.listServices()) {
                response.getWriter().println(name);
            }
        }

        response.flushBuffer();
    }
}
