package com.atlassian.plugin.spring.scanner.dynamic.contexts;

import com.atlassian.event.api.EventPublisher;
import org.osgi.framework.BundleContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * This class can install new Spring application contexts into a running Atlassian plugin
 */
public class DynamicContext {
    public interface Installer {
        /**
         * This method will create and install a new child application context into the plugin.
         * <p>
         * As a side effect a {@link com.atlassian.plugin.event.events.PluginRefreshedEvent} will be generated for this new
         * child context.
         * <p>
         * By calling this method you are able to have the plugin auto-wire components such as actions and web-items from
         * the child context instead of the originating parent context.  This will allow you to dynamically grow and shrink
         * the components that your plugin gives off to the world based on external events such as licencing or memory
         * passivisation.
         *
         * @param springConfigPaths the paths to find spring configuration files on the class path
         * @param parentContext     the parent context, typically the context that came with your plugin
         * @return the newly created child context
         */
        ConfigurableApplicationContext useContext(String[] springConfigPaths, ApplicationContext parentContext);

        /**
         * This method can "close" a child application context (typically one created with {@link #useContext(String[],
         * org.springframework.context.ApplicationContext)} and set in the replacement application context instead.
         * Typically the replacement would be the parent of the child context to be closed but it doesnt always have to be.
         *
         * @param childContext       the child context to close.
         * @param replacementContext the context to replace the child context with.  Typically this can be its parent
         */
        void closeAndUseContext(ConfigurableApplicationContext childContext, ApplicationContext replacementContext);
    }

    /**
     * Creates a new installer that can create and install {@link ApplicationContext}s into a running plugin
     *
     * @param eventPublisher the event publisher
     * @param bundleContext  the current plugins bundle context
     * @param pluginKey      the key of the current plugin
     * @return a new installer
     */
    public static Installer installer(final EventPublisher eventPublisher, final BundleContext bundleContext, final String pluginKey) {
        return new ApplicationContextInstallerImpl(eventPublisher, bundleContext, pluginKey);
    }
}
