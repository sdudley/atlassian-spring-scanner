package com.atlassian.plugin.spring.scanner.core;

import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.BitbucketComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.ConfluenceComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.FecruComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.RefappComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.StashComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.BitbucketImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.ConfluenceImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.FecruImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.RefappImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.StashImport;
import com.atlassian.plugin.spring.scanner.core.vfs.VirtualFile;
import com.atlassian.plugin.spring.scanner.core.vfs.VirtualFileFactory;
import com.atlassian.plugin.spring.scanner.util.CommonConstants;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static com.atlassian.plugin.spring.scanner.ProductFilter.BAMBOO;
import static com.atlassian.plugin.spring.scanner.ProductFilter.BITBUCKET;
import static com.atlassian.plugin.spring.scanner.ProductFilter.CONFLUENCE;
import static com.atlassian.plugin.spring.scanner.ProductFilter.FECRU;
import static com.atlassian.plugin.spring.scanner.ProductFilter.JIRA;
import static com.atlassian.plugin.spring.scanner.ProductFilter.REFAPP;
import static com.atlassian.plugin.spring.scanner.ProductFilter.STASH;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_DEV_EXPORT_KEY;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_EXPORT_KEY;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_IMPORT_KEY;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_KEY;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_PRIMARY_KEY;
import static java.util.Arrays.asList;


/**
 * This class writes out the component and import index files into their specific directories and specific index files
 * names
 * <p>
 * The Reflections / Javassist code deals only in strings and hence this is written as such.
 */
public class SpringIndexWriter {
    public static final List<String> KNOWN_PRODUCT_IMPORT_ANNOTATIONS = asList(
            BambooImport.class.getCanonicalName(),
            BitbucketImport.class.getCanonicalName(),
            ConfluenceImport.class.getCanonicalName(),
            FecruImport.class.getCanonicalName(),
            JiraImport.class.getCanonicalName(),
            RefappImport.class.getCanonicalName(),
            StashImport.class.getCanonicalName());

    private final Map<String, RecordedAnnotations> recordedProfiles = new HashMap<String, RecordedAnnotations>();
    private final VirtualFileFactory fileFactory;

    /**
     * A writer that can record and write index files of components
     *
     * @param baseDir the base directory to write to.  Typically this is the class file output directory
     */
    public SpringIndexWriter(final String baseDir) {
        // since we see EVERYTHING in the byte code scanner, we can always be clean
        // and start from scratch.
        cleanDirectory(new File(baseDir, CommonConstants.INDEX_FILES_DIR));

        fileFactory = new VirtualFileFactory(new File(baseDir));
    }

    public boolean isInteresting(String annotationType) {
        return (null != MeaningfulAnnotation.fromCanonicalName(annotationType));
    }

    public boolean isParameterOrFieldAnnotation(final String annotationType) {
        final MeaningfulAnnotation meaningfulAnnotation = MeaningfulAnnotation.fromCanonicalName(annotationType);
        return (null != meaningfulAnnotation) && meaningfulAnnotation.parameterOrFieldAnnotation;
    }

    public void encounteredAnnotation(Set<String> targetProfiles, String annotationType, String nameFromAnnotation, String className) {
        Set<String> profiles = new HashSet<String>(targetProfiles);
        if (profiles.isEmpty()) {
            // when we have no profiles, we go into the magic one called 'default'
            profiles.add(CommonConstants.DEFAULT_PROFILE_NAME);
        }

        for (String profile : profiles) {
            RecordedAnnotations recordedAnnotations = recordedProfiles.get(profile);
            if (recordedAnnotations == null) {
                recordedAnnotations = new RecordedAnnotations();
                recordedProfiles.put(profile, recordedAnnotations);
            }
            recordedAnnotations.record(annotationType, nameFromAnnotation, className);
        }

    }

    public void writeIndexes() {
        for (Map.Entry<String, RecordedAnnotations> annotationsEntry : recordedProfiles.entrySet()) {
            writeProfileIndexes(annotationsEntry.getKey(), annotationsEntry.getValue());
        }

    }

    private void writeProfileIndexes(final String profileName, final RecordedAnnotations annotations) {
        //
        // the javac annotation processing only allows you to open a file for input and output ONCE
        // for reasons you can look up online.  Basically so its knows what it has seen before.
        // So if you open a file, write to it and then try to repeat that it will blow up.
        //
        // So we need to collapse the map here so that we go from many annotations sharing the
        // same index file to a map of index file ---> all components for that index file
        //
        // and then write it out per unique file so they are only opened once.
        //
        Map<String, Set<String>> fileNameToComponents = new HashMap<String, Set<String>>();
        for (Map.Entry<MeaningfulAnnotation, Set<String>> entry : annotations.getRecordedAnnotations().entrySet()) {
            MeaningfulAnnotation meaningfulAnnotation = entry.getKey();
            if (meaningfulAnnotation.isWrittenToDisk()) {
                Set<String> perAnnotationComponents = entry.getValue();
                String indexFileName = meaningfulAnnotation.getFileName();
                Set<String> currentComponents = fileNameToComponents.get(indexFileName);
                if (currentComponents == null) {
                    currentComponents = new TreeSet<String>();
                    fileNameToComponents.put(indexFileName, currentComponents);
                }
                currentComponents.addAll(perAnnotationComponents);
            }
        }
        //
        // now that we have them in file name order we can write them safely under the javac world
        for (Map.Entry<String, Set<String>> entry : fileNameToComponents.entrySet()) {
            try {
                writeIndexFile(profileName, entry.getKey(), entry.getValue());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void writeIndexFile(final String profileName, final String indexFileName, final Set<String> entries)
            throws IOException {
        File file = makeProfiledFileName(profileName, indexFileName);

        VirtualFile vf = fileFactory.getFile(file.getPath());

        Set<String> lines = new TreeSet<String>();
        // read the existing lines
        lines.addAll(vf.readLines());
        lines.addAll(entries);
        vf.writeLines(lines);
    }

    private File makeProfiledFileName(final String profileName, String fileName) throws IOException {
        File file = new File(CommonConstants.PROFILE_PREFIX + profileName, fileName);
        if (CommonConstants.DEFAULT_PROFILE_NAME.equals(profileName)) {
            // the well known default profile goes into the top level directory
            // and only explicitly defined profiles go into sub directories
            file = new File(fileName);
        }
        return new File(CommonConstants.INDEX_FILES_DIR, file.getPath());
    }

    private void cleanDirectory(final File destination) {
        if (destination.exists()) {
            try {
                Files.walkFileTree(destination.toPath(), new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                        if (e != null) {
                            throw e;
                        }
                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (IOException e) {
                throw new RuntimeException("Unable to delete directory " + destination, e);
            }
        }
    }

    private enum MeaningfulAnnotation {
        // Well known spring annotations
        Component(COMPONENT_KEY, Component.class),
        Service(COMPONENT_KEY, Service.class),
        Controller(COMPONENT_KEY, Controller.class),
        Repository(COMPONENT_KEY, Repository.class),
        Primary(COMPONENT_PRIMARY_KEY, Primary.class),

        // JSR annotations
        Named(COMPONENT_KEY, Named.class, true),

        // Our component annotations
        ClasspathComponent(COMPONENT_KEY, ClasspathComponent.class, true),
        BambooComponent(BAMBOO.getPerProductFile(COMPONENT_KEY), BambooComponent.class, true),
        BitbucketComponent(BITBUCKET.getPerProductFile(COMPONENT_KEY), BitbucketComponent.class, true),
        ConfluenceComponent(CONFLUENCE.getPerProductFile(COMPONENT_KEY), ConfluenceComponent.class, true),
        JiraComponent(JIRA.getPerProductFile(COMPONENT_KEY), JiraComponent.class, true),
        FecruComponent(FECRU.getPerProductFile(COMPONENT_KEY), FecruComponent.class, true),
        RefappComponent(REFAPP.getPerProductFile(COMPONENT_KEY), RefappComponent.class, true),
        StashComponent(STASH.getPerProductFile(COMPONENT_KEY), StashComponent.class, true),

        // Our import annotations
        Imports(COMPONENT_IMPORT_KEY, ComponentImport.class, true),
        BambooImports(BAMBOO.getPerProductFile(COMPONENT_IMPORT_KEY), BambooImport.class, true),
        BitbucketImports(BITBUCKET.getPerProductFile(COMPONENT_IMPORT_KEY), BitbucketImport.class, true),
        ConfluenceImports(CONFLUENCE.getPerProductFile(COMPONENT_IMPORT_KEY), ConfluenceImport.class, true),
        FecruImports(FECRU.getPerProductFile(COMPONENT_IMPORT_KEY), FecruImport.class, true),
        JiraImports(JIRA.getPerProductFile(COMPONENT_IMPORT_KEY), JiraImport.class, true),
        RefappImports(REFAPP.getPerProductFile(COMPONENT_IMPORT_KEY), RefappImport.class, true),
        StashImports(STASH.getPerProductFile(COMPONENT_IMPORT_KEY), StashImport.class, true),

        // Our export annotations
        ExportAsService(COMPONENT_EXPORT_KEY, ExportAsService.class, true),
        ExportAsDevService(COMPONENT_DEV_EXPORT_KEY, ExportAsDevService.class, true),
        ModuleType(COMPONENT_EXPORT_KEY, ModuleType.class);

        private static final Map<String, MeaningfulAnnotation> canonicalNameIndex = Maps.uniqueIndex(
                ImmutableList.copyOf(values()),
                new Function<MeaningfulAnnotation, String>() {
                    @Override
                    public String apply(@Nullable final MeaningfulAnnotation meaningfulAnnotation) {
                        return meaningfulAnnotation.forAnnotation.getCanonicalName();
                    }
                });

        private final String fileName;
        private final Class forAnnotation;
        private final boolean parameterOrFieldAnnotation;

        MeaningfulAnnotation(final String fileName, final Class forAnnotation) {
            this(fileName, forAnnotation, false);
        }

        MeaningfulAnnotation(final Class forAnnotation) {
            this(null, forAnnotation, false);
        }

        MeaningfulAnnotation(final String fileName, final Class forAnnotation, boolean parameterOrFieldAnnotation) {
            this.fileName = fileName;
            this.forAnnotation = forAnnotation;
            this.parameterOrFieldAnnotation = parameterOrFieldAnnotation;
        }

        private static MeaningfulAnnotation fromCanonicalName(String annotationType) {
            return canonicalNameIndex.get(annotationType);
        }

        private String getFileName() {
            return fileName;
        }

        private boolean isWrittenToDisk() {
            return null != fileName;
        }
    }

    /**
     * Simple holder class to record sets of annotations as we traverse the classes
     */
    private class RecordedAnnotations {
        final Map<MeaningfulAnnotation, Set<String>> recordedAnnotations = new HashMap<MeaningfulAnnotation, Set<String>>();

        public Map<MeaningfulAnnotation, Set<String>> getRecordedAnnotations() {
            return recordedAnnotations;
        }

        public void record(final String annotationType, final String nameFromAnnotation, final String className) {
            StringBuilder sb = new StringBuilder(className);
            if (nameFromAnnotation != null) {
                String trimmed = nameFromAnnotation.trim();
                if (!trimmed.isEmpty()) {
                    sb.append("#").append(trimmed);
                }
            }
            final MeaningfulAnnotation annotation = MeaningfulAnnotation.fromCanonicalName(annotationType);
            if (null == annotation) {
                throw new IllegalStateException("Stop asking me for the impossible. Annotation " + annotationType + " not found");
            }

            addTo(annotation, sb.toString());
        }

        private void addTo(final MeaningfulAnnotation meaningfulAnnotation, final String value) {
            Set<String> values = recordedAnnotations.get(meaningfulAnnotation);
            if (values == null) {
                values = new TreeSet<String>();
                recordedAnnotations.put(meaningfulAnnotation, values);
            }
            values.add(value);
        }
    }


}
