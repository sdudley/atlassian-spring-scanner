package com.atlassian.plugin.spring.scanner.core;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.ParameterAnnotationsAttribute;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.ArrayMemberValue;
import javassist.bytecode.annotation.ClassMemberValue;
import javassist.bytecode.annotation.MemberValue;
import javassist.bytecode.annotation.StringMemberValue;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;

/**
 * A helper file of Javassist related code.
 *
 * The Reflections code gives us some help and an invocation framework however it turns out it doesn't quite give us enough and we
 * need to get down and dirty with Javassist.
 */
public class JavassistHelper {
    private static final Joiner ARRAY_MEMBER_VALUE_JOINER = Joiner.on(",");

    public List<String> getClassAnnotationNames(final ClassFile aClass) {
        return getAnnotationNames((AnnotationsAttribute) aClass.getAttribute(AnnotationsAttribute.visibleTag));
    }

    private List<String> getAnnotationNames(final AnnotationsAttribute... annotationsAttributes) {
        final List<String> result = Lists.newArrayList();

        if (annotationsAttributes != null) {
            for (final AnnotationsAttribute annotationsAttribute : annotationsAttributes) {
                if (annotationsAttribute != null) {
                    for (final Annotation annotation : annotationsAttribute.getAnnotations()) {
                        result.add(annotation.getTypeName());
                    }
                }
            }
        }

        return result;
    }

    /**
     * Returns a named "member" of the annotation as a string value
     *
     * @param classFile      the class to inspect
     * @param annotationType the name of the annotation type
     * @param memberName     the member to retrieve
     * @return a value or null if it has no member
     */
    Set<String> getAnnotationMemberSet(final ClassFile classFile, final String annotationType, final String memberName) {
        final MemberValue value = getMemberValue(classFile, annotationType, memberName);
        if (value != null && value instanceof ArrayMemberValue) {
            final Set<String> values = new HashSet<String>();
            final ArrayMemberValue arrayMemberValue = (ArrayMemberValue) value;
            final MemberValue[] arrayMemberValueValues = arrayMemberValue.getValue();
            for (final MemberValue memberValue : arrayMemberValueValues) {
                if (memberValue != null) {
                    values.add(removeQuotes(memberValue.toString()));
                }
            }
            return values;
        }
        return Collections.emptySet();
    }

    private MemberValue getMemberValue(final ClassFile classFile, final String annotationType, final String memberName) {
        final AnnotationsAttribute annotations = (AnnotationsAttribute) classFile.getAttribute(AnnotationsAttribute.visibleTag);
        final Annotation annotation = annotations.getAnnotation(annotationType);
        return annotation == null ? null : annotation.getMemberValue(memberName);
    }

    /*
     * Javassist gives us the annotations wrapped in the original quotes "
     */
    private String removeQuotes(String s) {
        if (s.startsWith("\"")) {
            s = s.substring(1);
        }
        if (s.endsWith("\"")) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }

    /**
     * Returns a named "member" of the annotation as a string value
     *
     * @param classFile      the class to inspect
     * @param annotationType the name of the annotation type
     * @param memberName     the member to retrieve
     * @return a value or null if it has no member
     */
    String getAnnotationMember(final ClassFile classFile, final String annotationType, final String memberName) {
        final MemberValue value = getMemberValue(classFile, annotationType, memberName);
        return dataForMemberValue(value);
    }

    /**
     * Return the member value of an annotation
     *
     * @param annotation the annotation to inspect
     * @param memberName the name of the annotation member
     * @return a value or null if it has no member
     */
    String getAnnotationMember(final Annotation annotation, final String memberName) {
        final MemberValue value = annotation == null ? null : annotation.getMemberValue(memberName);
        return dataForMemberValue(value);
    }

    private String dataForMemberValue(final MemberValue memberValue) {
        if (memberValue instanceof StringMemberValue) {
            return ((StringMemberValue) memberValue).getValue();
        } else if (memberValue instanceof ClassMemberValue) {
            return ((ClassMemberValue) memberValue).getValue();
        } else if (memberValue instanceof ArrayMemberValue) {
            final MemberValue[] entryValues = ((ArrayMemberValue) memberValue).getValue();
            final Iterable<String> entryData = transform(copyOf(entryValues), new Function<MemberValue, String>() {
                @Override
                public String apply(@Nullable final MemberValue entryValue) {
                    return dataForMemberValue(entryValue);
                }
            });
            return ARRAY_MEMBER_VALUE_JOINER.join(entryData);

        } else {
            // This branch is entered if memberValue is null, which happens when the annoation value is being defaulted.
            // The non-null branch is preserving legacy behaviour, as far as i can see no non null values not handled by previous
            // branches of the if can be generated by current code.
            return memberValue == null ? null : removeQuotes(memberValue.toString());
        }
    }

    /**
     * Returns the list of annotations a method parameter might have.
     *
     * @param method         the method to inspect
     * @param parameterIndex the index of the method parameter to look at
     * @return a list of parameter annotations it may have
     */
    List<Annotation> getParameterAnnotations(final MethodInfo method, final int parameterIndex) {
        final List<Annotation> result = Lists.newArrayList();

        final ParameterAnnotationsAttribute parameterAnnotationsAttribute =
                (ParameterAnnotationsAttribute) method.getAttribute(ParameterAnnotationsAttribute.visibleTag);
        if (parameterAnnotationsAttribute != null) {
            final Annotation[][] allAnnotations = parameterAnnotationsAttribute.getAnnotations();
            if (parameterIndex < allAnnotations.length) {
                final Annotation[] annotations = allAnnotations[parameterIndex];
                if (annotations != null) {
                    Collections.addAll(result, annotations);
                }
            }
        }
        return result;
    }


}
