package com.atlassian.plugin.spring.scanner.runtime.impl.util;

import org.junit.Assume;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.fail;

/**
 * Just a smoke test to catch if the classnames we use to determine which product we're in ever change
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductFilterUtilTest {
    @Test
    public void testBambooClassIsPresent() {
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_BAMBOO_CLASSPATH, "Bamboo");
    }

    @Test
    public void testBitbucketClassIsPresent() {
        Assume.assumeTrue(isAtLeastJavaVersion(8));
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_BITBUCKET_CLASSPATH, "Bitbucket");
    }

    @Test
    public void testConfluenceClassIsPresent() {
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_CONFLUENCE_CLASSPATH, "Confluence");
    }

    @Test
    public void testFecruClassIsPresent() {
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_FECRU_CLASSPATH, "Fecru");
    }

    @Test
    public void testJiraClassIsPresent() {
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_JIRA_CLASSPATH, "JIRA");
    }

    @Test
    public void testStashClassIsPresent() {
        Assume.assumeTrue(isAtLeastJavaVersion(7));
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_STASH_CLASSPATH, "Stash");
    }

    @Test
    public void testRefappClassIsPresent() {
        assertClassIsPresentOnClasspath(ProductFilterUtil.CLASS_ON_REFAPP_CLASSPATH, "Refapp");
    }

    private void assertClassIsPresentOnClasspath(String clazz, String product) {
        try {
            Class.forName(clazz);
        } catch (ClassNotFoundException e) {
            fail(String.format(
                    "Class %s not found on classpath, is it no longer exported from %s? If so, %s will need to be " +
                            "updated to use an exported class from %s.",
                    clazz, product, ProductFilterUtil.class.getName(), product));
        }
    }

    private boolean isAtLeastJavaVersion(int majorVersion) {
        Pattern pattern = Pattern.compile("(\\d+)\\.(\\d+)");
        Matcher matcher = pattern.matcher(System.getProperty("java.version"));
        if (matcher.find()) {
            int version = Integer.parseInt(matcher.group(2));
            return version >= majorVersion;
        }
        return false;
    }
}
