package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.springframework.stereotype.Component;

@ExportAsService
@Component
public class PublicServiceProxy implements Service {
    @Override
    public void a() {
    }
}
