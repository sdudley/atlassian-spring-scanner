package com.atlassian.plugin.spring.scanner.runtime.impl;

import com.atlassian.plugin.spring.scanner.util.CommonConstants;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.util.ClassUtils;

import java.beans.Introspector;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static com.atlassian.plugin.spring.scanner.runtime.impl.util.AnnotationIndexReader.getIndexFilesForProfiles;
import static com.atlassian.plugin.spring.scanner.runtime.impl.util.AnnotationIndexReader.readAllIndexFilesForProduct;
import static com.atlassian.plugin.spring.scanner.runtime.impl.util.AnnotationIndexReader.splitProfiles;
import static com.atlassian.plugin.spring.scanner.runtime.impl.util.BeanDefinitionChecker.needToRegister;

/**
 * This class is responsible for reading the class index files and generating bean definitions from them. We assume all
 * of the proper type checks/visibility checks have been done by the annotation processors. This means that if a class
 * is listed in an index, it qualifies as a bean candidate.
 */
public class ClassIndexBeanDefinitionScanner {
    private static final Logger log = LoggerFactory.getLogger(ClassIndexBeanDefinitionScanner.class);

    private final BeanDefinitionRegistry registry;
    private final String profileName;
    /**
     * A constant for {@link AbstractBeanDefinition#setAutowireMode(int)} used for beans created from the index,
     * or null if the autowire mode should not be set.
     */
    private final Integer autowireDefault;
    private final BundleContext bundleContext;

    public ClassIndexBeanDefinitionScanner(
            final BeanDefinitionRegistry registry,
            final String profileName,
            final Integer autowireDefault,
            final BundleContext bundleContext) {
        this.registry = registry;
        this.profileName = profileName;
        this.autowireDefault = autowireDefault;
        this.bundleContext = bundleContext;
    }

    /**
     * Gets the map of {@code beanName -> beanDefinition} and returns a set of bean definition holders
     *
     * @return a set of bean def holders
     */
    protected Set<BeanDefinitionHolder> doScan() {

        final Set<BeanDefinitionHolder> beanDefinitions = new LinkedHashSet<BeanDefinitionHolder>();
        final Map<String, BeanDefinition> namesAndDefinitions = findCandidateComponents();

        for (final Map.Entry<String, BeanDefinition> nameAndDefinition : namesAndDefinitions.entrySet()) {

            if (needToRegister(nameAndDefinition.getKey(), nameAndDefinition.getValue(), registry)) {
                final BeanDefinitionHolder definitionHolder = new BeanDefinitionHolder(
                        nameAndDefinition.getValue(), nameAndDefinition.getKey());
                beanDefinitions.add(definitionHolder);
                registerBeanDefinition(definitionHolder, registry);
            }
        }
        return beanDefinitions;
    }

    /**
     * Reads the components from the index file(s) and generates a map of {@code beanName -> beanDefinitions} for them
     *
     * @return a Map of bean names to bean definitions
     */
    public Map<String, BeanDefinition> findCandidateComponents() {
        final Map<String, BeanDefinition> candidates = new HashMap<String, BeanDefinition>();

        final Bundle bundle = bundleContext.getBundle();

        final Set<String> beanTypeAndNames = new TreeSet<String>();

        final String[] profileNames = splitProfiles(profileName);
        for (final String fileToRead : getIndexFilesForProfiles(profileNames, CommonConstants.COMPONENT_KEY)) {
            beanTypeAndNames.addAll(readAllIndexFilesForProduct(fileToRead, bundle, bundleContext));
        }
        final Set<String> primaryComponentTypeAndNames = findPrimaryComponentTypeAndNames(profileNames, bundle);

        for (final String beanTypeAndName : beanTypeAndNames) {
            final String[] typeAndName = beanTypeAndName.split("#");

            final String beanClassName = typeAndName[0];
            String beanName = "";

            if (typeAndName.length > 1) {
                beanName = typeAndName[1];
            }

            if (beanName.isEmpty()) {
                beanName = Introspector.decapitalize(ClassUtils.getShortName(beanClassName));
            }

            if (log.isDebugEnabled()) {
                log.debug(String.format("Found candidate bean '%s' from class '%s'", beanName, beanClassName));
            }

            final BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(beanClassName);
            if (null != autowireDefault) {
                beanDefinitionBuilder.setAutowireMode(autowireDefault);
            }

            final BeanDefinition beanDefinition = beanDefinitionBuilder.getBeanDefinition();
            if (primaryComponentTypeAndNames.contains(beanTypeAndName)) {
                beanDefinition.setPrimary(true);
            }
            candidates.put(beanName, beanDefinition);
        }

        return candidates;
    }

    /**
     * Reads the components from the primary-component index file(s)
     *
     * @return a Set containing all of the beanTypeAndNames
     * @param profileNames
     * @param bundle
     */
    private Set<String> findPrimaryComponentTypeAndNames(String[] profileNames, Bundle bundle) {
        final Set<String> beanTypeAndNames = new HashSet<>();
        for (final String fileToRead : getIndexFilesForProfiles(profileNames, CommonConstants.COMPONENT_PRIMARY_KEY)) {
            beanTypeAndNames.addAll(readAllIndexFilesForProduct(fileToRead, bundle, bundleContext));
        }
        return beanTypeAndNames;
    }

    /**
     * copyPasta from spring-context:component-scan classes
     */
    protected void registerBeanDefinition(final BeanDefinitionHolder definitionHolder, final BeanDefinitionRegistry registry) {
        BeanDefinitionReaderUtils.registerBeanDefinition(definitionHolder, registry);
    }
}
