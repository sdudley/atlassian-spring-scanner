package com.atlassian.plugin.spring.scanner.annotation.imports;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation representing an OSGi service that's required to be imported into this bundle.
 * Can be applied to constructor params where the param type is a service interface exported
 * by another bundle
 */
@Target({ElementType.PARAMETER, ElementType.ANNOTATION_TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ComponentImport {

    /**
     * Override the default spring bean name to assign <b>within this plugin</b> for the imported OSGi service.
     * Not usually needed.
     * <p>
     * The default is to use the imported interface name with the first character lowercased,
     * e.g. this code will result in a bean name of {@code contentService}:
     * <pre><code>
     * &#64;Named
     * public class MyComponent {
     *     &#64;Inject
     *     public MyComponent(&#64;ComponentImport ContentService myExternalService) {
     *         // more stuff here
     *     }
     * }
     * </code></pre>
     * So you only need to set this if you want to override the default: e.g. if you're importing
     * two services with the same classname from different packages, you need to set different names for them;
     * or if you need specific bean names to use with {@code @Qualifier("yourBeanName")}.
     * <p>
     * This name is local to this plugin's spring context;
     * it's unrelated to the name of the bean (inside the product's or the providing plugin's spring context)
     * that's exporting the service we're importing.
     */
    String value() default "";
}
