package com.atlassian.plugin.spring.scanner.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for a type that indicates what profiles it belongs to
 */
@Target({ElementType.TYPE, ElementType.PACKAGE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Profile {
    /**
     * The set of profiles for which this component should be registered.
     */
    String[] value() default "default";
}
